#include <stdio.h>

#include "thread.h"
#include "xtimer.h"
#include "board_common.h"

static char stack[THREAD_STACKSIZE_MAIN];


static void *thread_handler0(void *arg)
{
    (void)arg;
    while (1) {
        LED0_TOGGLE;
        xtimer_sleep(2);
    }
    return NULL;
}

static void *thread_handler1(void *arg)
{
    (void)arg;
    while (1) {
        LED1_TOGGLE;
        xtimer_sleep(1);
    }
    return NULL;
}

static void *thread_handler2(void *arg)
{
    (void)arg;
    while (1) {
        LED2_TOGGLE;
        xtimer_sleep(0.5);
    }
    return NULL;
}

int main(void)
{
    thread_create(stack, sizeof(stack), THREAD_PRIORITY_MAIN + 1,
                  0, thread_handler0, NULL, "thread0");
    thread_create(stack, sizeof(stack), THREAD_PRIORITY_MAIN + 1,
                  0, thread_handler1, NULL, "thread1");
    thread_create(stack, sizeof(stack), THREAD_PRIORITY_MAIN + 1,
                  0, thread_handler2, NULL, "thread2");
    /* bloucle principale */
    while (1) {
	    
        xtimer_sleep(4);
    } 
    return 0;
}
